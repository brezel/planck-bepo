# Layout BÉPO pour les claviers Planck

**ATTENTION** : Uniquement pour `Rev 6`, autrement le firmware sera trop gros et ne pourra pas être flashé.

![](https://gitlab.com/brezel/public/bepo/planck-bepo/raw/master/doc/layout.png)
