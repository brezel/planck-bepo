
#ifndef CONFIG_USER_H
#define CONFIG_USER_H

#include "../../config.h"

#ifdef AUDIO_ENABLE
  #define STARTUP_SONG SONG(MARIO_THEME)
  #define GOODBYE_SONG SONG(MARIO_GAMEOVER)
#endif

#endif
